import React, { FunctionComponent as Component } from "react"
import { observer } from "mobx-react-lite"
import { ViewStyle, TextStyle, View } from "react-native"
import { Screen, Text, Header, Wallpaper } from "../components"
import { useNavigation } from "@react-navigation/native"
// import { useStores } from "../models"
import { color, spacing } from "../theme"
import { Content, ListItem, CheckBox, Body } from "native-base"

const FULL: ViewStyle = { flex: 1 }
const CONTAINER: ViewStyle = {
  backgroundColor: color.transparent,
  paddingHorizontal: spacing[4],
}
const BOLD: TextStyle = { fontWeight: "bold" }
const HEADER: TextStyle = {
  paddingTop: spacing[3],
  paddingBottom: spacing[5] - 1,
  paddingHorizontal: 0,
}
const HEADER_TITLE: TextStyle = {
  ...BOLD,
  fontSize: 12,
  lineHeight: 15,
  textAlign: "center",
  letterSpacing: 1.5,
}
const TITLE: TextStyle = {
  ...BOLD,
  fontSize: 28,
  lineHeight: 38,
  textAlign: "center",
  marginBottom: spacing[5],
}

export const AwesomeScreen: Component = observer(function AwesomeScreen() {

  const navigation = useNavigation()
  const goBack = () => navigation.goBack()

  // Pull in one of our MST stores
  // const { someStore, anotherStore } = useStores()
  // OR
  // const rootStore = useStores()

  // Pull in navigation via hook
  // const navigation = useNavigation()
  return (
    <View style={FULL}>
      <Wallpaper />
      <Screen style={CONTAINER} preset="scroll" backgroundColor={color.transparent}>
        <Header
            headerTx="demoScreen.howTo"
            leftIcon="back"
            onLeftPress={goBack}
            style={HEADER}
            titleStyle={HEADER_TITLE}
          />
          <Text style={TITLE} preset="header" tx="demoScreen.title" />
        <Text preset="header" tx="awesomeScreen.header"/>

          <Content>
            <ListItem>
              <CheckBox checked={true} />
              <Body>
                <Text>Daily Stand Up</Text>
              </Body>
            </ListItem>
            <ListItem>
              <CheckBox checked={false} />
              <Body>
                <Text>Discussion with Client</Text>
              </Body>
            </ListItem>
            <ListItem>
              <CheckBox checked={false} color="green"/>
              <Body>
                <Text>Finish list Screen</Text>
              </Body>
            </ListItem>
          </Content>

      </Screen>
    </View>
  )
})
